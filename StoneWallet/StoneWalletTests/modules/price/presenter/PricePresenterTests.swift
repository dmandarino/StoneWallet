//
//  PricePresenterTests.swift
//  StoneWalletTests
//
//  Created by Douglas Pinheiro Mandarino on 04/01/18.
//  Copyright © 2018 Douglas Mandarino. All rights reserved.
//

import XCTest
@testable import StoneWallet

class PricePresenterTests: XCTestCase {

    var sut: PricePresenter?

    override func setUp() {
        super.setUp()
        sut = PricePresenter()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testFetchBitcoin() {
        let expectation = XCTestExpectation(description: "Test call model to fetch Bitcoin")
        sut?.model = PriceModelMock(expectation: expectation)
        sut?.fetchCoin()
        wait(for: [expectation], timeout: 10)
    }

    func testDidFetchBitcoin() {
        let expectation = XCTestExpectation(description: "Test call view present Bitcoin")
        let view = PriceViewMock(expectation: expectation)
        sut?.output = view
        sut?.presentBitCoinValues(Bitcoin(high: 0, low: 0, last: 0, buy: 0, sell: 0))
        wait(for: [expectation], timeout: 10)
    }

    func testUpdateAmount() {
        let expectation = XCTestExpectation(description: "Test update Amount")
        let view = PriceViewMock(expectation: expectation)
        sut?.output = view
        sut?.presentWallet(withAmount: 10, bitcoinQtd: 5, andRealQtd: 10)
        wait(for: [expectation], timeout: 10)
    }
}

private class PriceModelMock: PriceModelProtocol {

    var output: PriceModelOutputProtocol?
    private let expectation: XCTestExpectation?

    init(expectation: XCTestExpectation) {
        self.expectation = expectation
    }

    func fetchBitcoin() {
        expectation?.fulfill()
    }
}

private class PriceViewMock: PricePresenterOutputProtocol {

    private let expectation: XCTestExpectation?

    init(expectation: XCTestExpectation) {
        self.expectation = expectation
    }

    func updateWalletValues(amount: Float, bitcoinQtd: Float, realQtd: Float) {
        if expectation?.description == "Test update Amount" {
            expectation?.fulfill()
        }
    }

    func showCoinValues(withValues values: [[String: Float]]) {
        if expectation?.description == "Test call view present Bitcoin" {
            expectation?.fulfill()
        }
    }
}
