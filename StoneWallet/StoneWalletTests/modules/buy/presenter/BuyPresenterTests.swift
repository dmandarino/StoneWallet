//
//  BuyPresenterTests.swift
//  StoneWalletTests
//
//  Created by Douglas Pinheiro Mandarino on 15/01/18.
//  Copyright © 2018 Douglas Mandarino. All rights reserved.
//

import XCTest
@testable import StoneWallet

class BuyPresenterTests: XCTestCase {

    var sut: BuyPresenter?

    override func setUp() {
        super.setUp()
        sut = BuyPresenter()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testFetchBitcoin() {
        let expectation = XCTestExpectation(description: "Test call model to fetch Bitcoin")
        sut?.model = BuyModelMock(expectation: expectation)
        sut?.fetchCoin()
        wait(for: [expectation], timeout: 10)
    }

    func testDidFetchBitcoin() {
        let expectation = XCTestExpectation(description: "Test call view present Bitcoin")
        let view = BuyViewMock(expectation: expectation)
        sut?.output = view
        sut?.presentBitCoinValues(Bitcoin(high: 0, low: 0, last: 0, buy: 0, sell: 0))
        wait(for: [expectation], timeout: 10)
    }

    func testShowAmount() {
        let expectation = XCTestExpectation(description: "Test call view show amount" )
        let view = BuyViewMock(expectation: expectation)
        sut?.output = view
        sut?.presentAmount(withValue: 10)
        wait(for: [expectation], timeout: 10)
    }

    func testBuyBitcoin() {
        let expectation = XCTestExpectation(description: "Test call model to buy coin" )
        let model = BuyModelMock(expectation: expectation)
        sut?.model = model
        sut?.buyCoin(quantity: "34")
        wait(for: [expectation], timeout: 10)
    }

    func testValidateBuyBitcoin() {
        let model = BuyModelBuyAndSellMock()
        sut?.model = model
        sut?.buyCoin(quantity: "34")
    }

    func testValidateSellBitcoin() {
        let model = BuyModelBuyAndSellMock()
        sut?.model = model
        sut?.sellCoin(quantity: "35")
    }

    func testInvalidInputShouldNotBrokenBitcoin() {
        let model = BuyModelBuyAndSellMock()
        sut?.model = model
        sut?.sellCoin(quantity: "ASDSASDASDAD")
        sut?.buyCoin(quantity: "ASDSASDASDAD")
    }
}

private class BuyModelBuyAndSellMock: BuyModelProtocol {

    var output: BuyModelOutputProtocol?

    func fetchBitcoin() {}

    func buyCoin(quantity: Float) {
        XCTAssertEqual(Float(34), quantity)
    }

    func sellCoin(quantity: Float) {
        XCTAssertEqual(Float(35), quantity)
    }
}

private class BuyModelMock: BuyModelProtocol {

    var output: BuyModelOutputProtocol?
    private let expectation: XCTestExpectation!

    init(expectation: XCTestExpectation) {
        self.expectation = expectation
    }

    func fetchBitcoin() {
        if expectation.debugDescription == "Test call model to fetch Bitcoin" {
            expectation?.fulfill()
        }
    }

    func buyCoin(quantity: Float) {
        if expectation.debugDescription == "Test call model to buy coin" {
            expectation?.fulfill()
        }
    }

    func sellCoin(quantity: Float) {
        if expectation.debugDescription == "Test call model to sell coin" {
            expectation?.fulfill()
        }
    }
}

private class BuyViewMock: BuyPresenterOutputProtocol {

    private let expectation: XCTestExpectation!

    init(expectation: XCTestExpectation) {
        self.expectation = expectation
    }

    func showBitcoinValue(withValues values: [String : Float]) {
        if expectation.debugDescription == "Test call view present Bitcoin" {
            expectation?.fulfill()
        }
    }

    func showAmount(value: Float) {
        if expectation.debugDescription == "Test call view show amount" {
            expectation?.fulfill()
        }
    }
}
