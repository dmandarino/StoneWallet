//
//  Connector.swift
//  StoneWallet
//
//  Created by Douglas Mandarino on 21/12/17.
//  Copyright © 2017 Douglas Mandarino. All rights reserved.
//

import Foundation
import Alamofire

protocol ConnectorProtocol {
    func fetch(fromEndPoint endpoint: String, completion: @escaping (Data?) -> Void)
}

class Connector: ConnectorProtocol {
    func fetch(fromEndPoint endpoint: String, completion: @escaping (Data?) -> Void) {
        Alamofire.request(endpoint, method: .get)
        .responseJSON { response in
            switch response.result {
            case .success:
                return completion(response.data)
            case .failure(let error):
                print(error)
            }
        }
    }
}
