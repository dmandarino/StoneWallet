//
//  PriceViewController.swift
//  StoneWallet
//
//  Created by Douglas Mandarino on 25/12/17.
//  Copyright © 2017 Douglas Mandarino. All rights reserved.
//

import Foundation
import UIKit

class PriceViewController: UIViewController, PriceViewProtocol {

    var presenter: PricePresenterProtocol?
    private var coinValues: [[String: Float]]?

    @IBOutlet var coinCollectionView: UICollectionView!
    @IBOutlet weak var amountLabel: UILabel!
    @IBOutlet weak var bitcoinQtdLabel: UILabel!
    @IBOutlet weak var realQtdLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }

    override func viewDidAppear(_ animated: Bool) {
        presenter?.fetchCoin()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    private func configure() {
        let configurator: PriceConfiguratorProtocol = PriceConfigurator()
        configurator.configure(viewController: self)
        self.coinCollectionView.register(UINib.init(nibName: "CoinCell", bundle: nil),
                                         forCellWithReuseIdentifier: "CoinCellIdentifier")
        self.coinCollectionView.dataSource = self
        self.coinCollectionView.delegate = self
    }
}

// MARK: - PricePresenterOutputProtocol

extension PriceViewController: PricePresenterOutputProtocol {

    func showCoinValues(withValues values: [[String: Float]]) {
        self.coinValues = values
        self.coinCollectionView.reloadData()
    }

    func updateWalletValues(amount: Float, bitcoinQtd: Float, realQtd: Float) {
        self.amountLabel.text = amount.asCurrency
        self.bitcoinQtdLabel.text = bitcoinQtd.asCurrency
        self.realQtdLabel.text = realQtd.asCurrency
    }
}

// MARK: - UICollectionViewDataSource & UICollectionViewDelegate

extension PriceViewController: UICollectionViewDataSource, UICollectionViewDelegate {

    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let number = coinValues?.count else {
            return 1
        }
        return number
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if let coinCell: CoinCell = collectionView.dequeueReusableCell(withReuseIdentifier: "CoinCellIdentifier",
                                                                       for: indexPath) as? CoinCell {
            if coinValues != nil {
                coinCell.lastValue.text = coinValues?[indexPath.item]["last"]?.asCurrency
                coinCell.sellValue.text = coinValues?[indexPath.item]["sell"]?.asCurrency
                coinCell.buyValue.text = coinValues?[indexPath.item]["buy"]?.asCurrency
            }
            return coinCell
        }
        return CoinCell()
    }
}
