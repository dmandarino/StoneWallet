//
//  EnvironmentTests.swift
//  StoneWalletTests
//
//  Created by Douglas Mandarino on 22/12/17.
//  Copyright © 2017 Douglas Mandarino. All rights reserved.
//

import XCTest
@testable import StoneWallet

class ConnectorTests: XCTestCase {

    var sut: Connector?

    override func setUp() {
        super.setUp()
        sut = Connector()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testFetchCoin() {
        let endpoint = "https://www.mercadobitcoin.net/api/BTC/ticker/"
        sut?.fetch(fromEndPoint: endpoint, completion: { data in
            XCTAssertNotNil(data)
        })
    }
}
