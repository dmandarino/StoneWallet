//
//  BuyModel.swift
//  StoneWallet
//
//  Created by Douglas Pinheiro Mandarino on 14/01/18.
//  Copyright © 2018 Douglas Mandarino. All rights reserved.
//

// MARK: - BuyModel

class BuyModel {

    var output: BuyModelOutputProtocol?
    var wallet: Wallet?
    private var worker: FetchCoinWorkerProtocol?
    private var walletWorker: WalletWorkerProtocol?
    private var bitcoin: Bitcoin?

    init() {
        self.walletWorker = WalletWorker()
        self.worker = FetchCoinWorker(delegate: self)
    }

    init(worker: FetchCoinWorkerProtocol, bitcoin: Bitcoin, walletWorker: WalletWorkerProtocol) {
        self.walletWorker = walletWorker
        self.worker = worker
        self.bitcoin = bitcoin
    }
}

// MARK: - BuyModelOutputProtocol

extension BuyModel: BuyModelProtocol {

    func fetchBitcoin() {
        self.worker?.fetchBitcoin()
    }

    func buyCoin(quantity: Float) {
        wallet = getWallet()
        if let buyValue = bitcoin?.buy, let currency = wallet?.currency {
            if quantity <= currency {
                let coinQuantityWanted = quantity/buyValue
                wallet?.bitcoinQtd += coinQuantityWanted
                wallet?.currency -= quantity
                walletWorker?.saveWallet(wallet!)
                presentAmount()
            }
        }
    }

    func sellCoin(quantity: Float) {
        wallet = getWallet()
        if let sellValue = bitcoin?.sell, let bitcoinQtd = wallet?.bitcoinQtd {
            if quantity <= bitcoinQtd {
                wallet?.bitcoinQtd -= quantity
                wallet?.currency += quantity * sellValue
                walletWorker?.saveWallet(wallet!)
                presentAmount()
            }
        }
    }

    private func getWallet() -> Wallet {
        guard let myWallet = walletWorker?.getWallet() else {
            return Wallet(bitcoinQtd: 0, currency: 100000)
        }
        return myWallet
    }
}

// MARK: - FetchCoinWorkerOutputProtocol

extension BuyModel: FetchCoinWorkerOutputProtocol {

    func didFetchBitcoin(bitcoin: Bitcoin) {
        self.bitcoin = bitcoin
        output?.presentBitCoinValues(bitcoin)
        presentAmount()
    }

    private func presentAmount() {
        let amount = calculateWalletAmount()
        output?.presentAmount(withValue: amount)
    }

    private func calculateWalletAmount() -> Float {
        guard let bitcoinValue = bitcoin?.last else {
            return 0
        }

        let wallet = walletWorker?.getWallet()

        guard let amount = wallet?.getAmount(withBitcoinValue: bitcoinValue) else {
            return 0
        }

        return amount
    }
}
