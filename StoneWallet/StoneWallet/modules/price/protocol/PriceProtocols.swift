//
//  PriceProtocols.swift
//  StoneWallet
//
//  Created by Douglas Mandarino on 25/12/17.
//  Copyright © 2017 Douglas Mandarino. All rights reserved.
//

// MARK: - Model

protocol PriceModelProtocol {
    weak var output: PriceModelOutputProtocol? { get set }

    func fetchBitcoin()
}

protocol PriceModelOutputProtocol: class {
    func presentBitCoinValues(_ bitcoin: Bitcoin)
    func presentWallet(withAmount amount: Float, bitcoinQtd: Float, andRealQtd realQtd: Float)
}

// MARK: - Presenter

protocol PricePresenterProtocol {
    var model: PriceModelProtocol? { get set }
    weak var output: PricePresenterOutputProtocol? { get set }

    func fetchCoin()
}

protocol PricePresenterOutputProtocol: class {
    func showCoinValues(withValues values: [[String: Float]])
    func updateWalletValues(amount: Float, bitcoinQtd: Float, realQtd: Float)
}

// MARK: - View

protocol PriceViewProtocol {
    var presenter: PricePresenterProtocol? { get set }
}

// MARK: - Configurator

protocol PriceConfiguratorProtocol {
    func configure(viewController: PriceViewController)
}
