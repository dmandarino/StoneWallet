//
//  Extensions.swift
//  StoneWallet
//
//  Created by Douglas Pinheiro Mandarino on 16/01/18.
//  Copyright © 2018 Douglas Mandarino. All rights reserved.
//

import Foundation

extension Float {
    var asCurrency: String {
        return String(format: "R$%.02f", self)
    }
}
