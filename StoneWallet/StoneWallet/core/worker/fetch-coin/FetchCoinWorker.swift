//
//  FetchCoinWorker.swift
//  StoneWallet
//
//  Created by Douglas Pinheiro Mandarino on 14/01/18.
//  Copyright © 2018 Douglas Mandarino. All rights reserved.
//

import Foundation
import SwiftyPlistManager
import SwiftyJSON

class FetchCoinWorker: FetchCoinWorkerProtocol {

    private let connector: ConnectorProtocol?
    private weak var delegate: FetchCoinWorkerOutputProtocol?
    private let mercadoBitCoinURL = "MercadoBitCoinURL"

    init(delegate: FetchCoinWorkerOutputProtocol) {
        self.delegate = delegate
        self.connector = Connector()
    }

    init(delegate: FetchCoinWorkerOutputProtocol, connector: ConnectorProtocol) {
        self.delegate = delegate
        self.connector = connector
    }

    func fetchBitcoin() {
        let endpoint = getMercadoBitcoinEndpoint()
        connector?.fetch(fromEndPoint: endpoint, completion: didFetchBitcoin)
    }

    private func didFetchBitcoin(responseData: Data?) {
        guard let data = responseData else {
            return
        }
        let bitcoin = parseBitcoin(data: data)
        delegate?.didFetchBitcoin(bitcoin: bitcoin)
    }

    private func parseBitcoin(data: Data) -> Bitcoin {
        do {
            let json = try JSON(data: data )
            let bitcoinValues = json["ticker"]
            return Bitcoin(high: bitcoinValues["high"].floatValue,
                           low: bitcoinValues["low"].floatValue,
                           last: bitcoinValues["last"].floatValue,
                           buy: bitcoinValues["buy"].floatValue,
                           sell: bitcoinValues["sell"].floatValue)
        } catch {

        }
        return Bitcoin(high: 0, low: 0, last: 0, buy: 0, sell: 0)
    }

    private func getMercadoBitcoinEndpoint() -> String {
        if let endpoint = SwiftyPlistManager.shared.fetchValue(for: mercadoBitCoinURL,
                                                               fromPlistWithName: "Environment") as? String {
            return endpoint
        }
        return ""
    }
}
