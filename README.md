# Stone Wallet

Stone Wallet is an iOS app (written in Swift) that allows you to check both Bitcoin's. It also allow you to keep your money as a wallet, buy Bitcoin, and sell it.


### Requeriments

For this project You'll need:
 `CocoaPods`
 `Fastlane`


### Versioning

In this project, I'm using `Gitflow`. So, please, whe you want to check the most recent version, check branch `develop`. `Master` has the most recent version delivered.

### How to start

First of all, run `pod install` in yor terminal in root folder.
Then, open Xcode using `StoneWallet.xcworkspace`.

### Features

 - See Bitcoin's value`s
 - Check your Wallet
 - Buy and Sell Bitcoin
 - Tests
    
 Coming Soon:
  - Show transactions
  - Show Brita's
  - Buy, sell and exchance between every coin

#### Credits

- Douglas Mandarino
- icons8

