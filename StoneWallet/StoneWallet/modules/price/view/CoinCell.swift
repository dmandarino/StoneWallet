//
//  CoinCell.swift
//  StoneWallet
//
//  Created by Douglas Mandarino on 01/01/18.
//  Copyright © 2018 Douglas Mandarino. All rights reserved.
//

import Foundation
import UIKit

class CoinCell: UICollectionViewCell {

    @IBOutlet var coinName: UILabel!
    @IBOutlet var buyValue: UILabel!
    @IBOutlet var sellValue: UILabel!
    @IBOutlet var lastValue: UILabel!

    override func awakeFromNib() {
        self.layer.cornerRadius = 8
        self.clipsToBounds = true

        layer.shadowColor = UIColor.black.cgColor
        layer.shadowOffset = CGSize(width: 8, height: 8)
        layer.shadowRadius = 8
        layer.shadowOpacity = 0.8
    }
}
