//
//  Bitcoin.swift
//  StoneWallet
//
//  Created by Douglas Mandarino on 25/12/17.
//  Copyright © 2017 Douglas Mandarino. All rights reserved.
//

import Foundation

struct Bitcoin {
    var high: Float
    var low: Float
    var last: Float
    var buy: Float
    var sell: Float
}
