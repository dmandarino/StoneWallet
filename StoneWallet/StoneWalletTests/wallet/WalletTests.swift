//
//  WalletTests.swift
//  StoneWallet
//
//  Created by Douglas Pinheiro Mandarino on 04/01/18.
//  Copyright © 2018 Douglas Mandarino. All rights reserved.
//

import XCTest
@testable import StoneWallet

class WalletTests: XCTestCase {

    var sut: Wallet?

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testCreateWallet() {
        sut = Wallet(bitcoinQtd: 0, currency: 100000)
        XCTAssertEqual(sut?.currency, Float(100000))
        XCTAssertEqual(sut?.bitcoinQtd, Float(0))
    }

    func testGetAmountValue() {
        sut = Wallet(bitcoinQtd: 10, currency: 1000)
        let amount = sut?.getAmount(withBitcoinValue: 10)
        XCTAssertEqual(amount, Float(1100))
    }

}
