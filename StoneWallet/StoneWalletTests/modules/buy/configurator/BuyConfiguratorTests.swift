//
//  BuyConfiguratorTests.swift
//  StoneWalletTests
//
//  Created by Douglas Pinheiro Mandarino on 14/01/18.
//  Copyright © 2018 Douglas Mandarino. All rights reserved.
//

import XCTest
@testable import StoneWallet

class BuyConfiguratorTests: XCTestCase {

    var sut: BuyConfiguratorProtocol?

    override func setUp() {
        super.setUp()
        sut = BuyConfigurator()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testBuyStackConfiguration() {
        let viewController = BuyViewController()
        sut?.configure(viewController: viewController)

        XCTAssertNotNil(viewController.presenter)
        XCTAssertNotNil(viewController.presenter?.model)
        XCTAssertNotNil(viewController.presenter?.model?.output)
        XCTAssertNotNil(viewController.presenter?.output)
    }
}
