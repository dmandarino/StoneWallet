//
//  PriceModelTests.swift
//  StoneWalletTests
//
//  Created by Douglas Mandarino on 30/12/17.
//  Copyright © 2017 Douglas Mandarino. All rights reserved.
//

import XCTest
@testable import StoneWallet

class PriceModelTests: XCTestCase {

    var sut: PriceModel?

    override func setUp() {
        super.setUp()
        sut = PriceModel()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testFetchBitcoin() {
        let expectation = XCTestExpectation(description: "Test call worker to fetch Bitcoin")
        let worker = WorkerMock(expectation: expectation)
        sut = PriceModel(worker: worker)
        sut?.fetchBitcoin()
        wait(for: [expectation], timeout: 10)
    }

    func testDidFetchBitcoin() {
        let expectation = XCTestExpectation(description: "Test call model to present Bitcoin")
        let presenter = PricePresenterMock(expectation: expectation)
        sut?.output = presenter
        sut?.fetchBitcoin()
        wait(for: [expectation], timeout: 10)
    }

    func testDidFetchBitcoinAndShowWalletAmount() {
        let expectation = XCTestExpectation(description: "Test call model to present Wallet Amount")
        let presenter = PricePresenterMock(expectation: expectation)
        sut?.output = presenter
        sut?.fetchBitcoin()
        wait(for: [expectation], timeout: 10)
    }
}

private class WorkerMock: FetchCoinWorkerProtocol {

    private let expectation: XCTestExpectation?

    init(expectation: XCTestExpectation) {
        self.expectation = expectation
    }

    func fetchBitcoin() {
        expectation?.fulfill()
    }

}

private class PricePresenterMock: PriceModelOutputProtocol {

    private let expectation: XCTestExpectation?

    init(expectation: XCTestExpectation) {
        self.expectation = expectation
    }

    func presentWallet(withAmount amount: Float, bitcoinQtd: Float, andRealQtd realQtd: Float) {
        if expectation?.description == "Test call model to present Wallet Amount" {
            expectation?.fulfill()
        }
    }

    func presentBitCoinValues(_ bitcoin: Bitcoin) {
        if expectation?.description == "Test call model to present Bitcoin" {
            expectation?.fulfill()
        }
    }
}
