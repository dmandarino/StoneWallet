//
//  BuyConfigurator.swift
//  StoneWallet
//
//  Created by Douglas Pinheiro Mandarino on 14/01/18.
//  Copyright © 2018 Douglas Mandarino. All rights reserved.
//

import Foundation

// MARK: - BuyConfiguratorProtocol

struct BuyConfigurator: BuyConfiguratorProtocol {

    func configure(viewController: BuyViewController) {
        let presenter = BuyPresenter()
        let model = BuyModel()

        viewController.presenter = presenter
        presenter.model = model
        model.output = presenter
        presenter.output = viewController
    }
}
