//
//  PriceModel.swift
//  StoneWallet
//
//  Created by Douglas Mandarino on 25/12/17.
//  Copyright © 2017 Douglas Mandarino. All rights reserved.
//

import Foundation

// MARK: - PriceModel

class PriceModel {

    weak var output: PriceModelOutputProtocol?
    private var fetchCoinWorker: FetchCoinWorkerProtocol?
    private var walletWorker: WalletWorkerProtocol?
    private var bitcoin: Bitcoin?

    init() {
        self.fetchCoinWorker = FetchCoinWorker(delegate: self)
        self.walletWorker = WalletWorker()
    }

    init(worker: FetchCoinWorkerProtocol) {
        self.fetchCoinWorker = worker
        self.walletWorker = WalletWorker()
    }
}

// MARK: - PriceModelProtocol

extension PriceModel: PriceModelProtocol {

    func fetchBitcoin() {
        self.fetchCoinWorker?.fetchBitcoin()
    }
}

// MARK: - FetchCoinWorkerOutputProtocol

extension PriceModel: FetchCoinWorkerOutputProtocol {

    func didFetchBitcoin(bitcoin: Bitcoin) {
        if let wallet = walletWorker?.getWallet() {
            let bitcoinValue = bitcoin.last
            let walletAmount = wallet.getAmount(withBitcoinValue: bitcoinValue)
            output?.presentBitCoinValues(bitcoin)
            output?.presentWallet(withAmount: walletAmount, bitcoinQtd: wallet.bitcoinQtd, andRealQtd: wallet.currency)
        }
    }
}
