//
//  Wallet.swift
//  StoneWallet
//
//  Created by Douglas Pinheiro Mandarino on 04/01/18.
//  Copyright © 2018 Douglas Mandarino. All rights reserved.
//

struct Wallet {
    var bitcoinQtd: Float
    var currency: Float

    func getAmount(withBitcoinValue bitcoinValue: Float) -> Float {
        return bitcoinValue * bitcoinQtd + currency
    }
}
