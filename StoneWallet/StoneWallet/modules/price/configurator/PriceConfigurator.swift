//
//  PriceConfigurator.swift
//  StoneWallet
//
//  Created by Douglas Mandarino on 25/12/17.
//  Copyright © 2017 Douglas Mandarino. All rights reserved.
//

import Foundation

// MARK: - PriceConfiguratorProtocol

struct PriceConfigurator: PriceConfiguratorProtocol {

    func configure(viewController: PriceViewController) {
        let presenter = PricePresenter()
        let model = PriceModel()

        viewController.presenter = presenter
        presenter.model = model
        model.output = presenter
        presenter.output = viewController
    }
}
