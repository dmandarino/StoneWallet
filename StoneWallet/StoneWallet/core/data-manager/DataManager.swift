//
//  DataManager.swift
//  StoneWallet
//
//  Created by Douglas Pinheiro Mandarino on 16/01/18.
//  Copyright © 2018 Douglas Mandarino. All rights reserved.
//

import UIKit

enum DataKey: String {
    case bitcoinQtd = "BITCOIN_QTD"
    case currency = "CURRENCY"
    case firstTime = "FIRST_TIME"
}

protocol DataManagerProtocol {
    func save(value: Float, forKey key: DataKey)
    func getValue(forKey key: DataKey) -> Float
    func isNotFirstTime() -> Bool
    func didPassFirstTime()
}

class DataManager: DataManagerProtocol {

    private let userDefaults: UserDefaults

    init() {
        self.userDefaults = UserDefaults.standard
    }

    func save(value: Float, forKey key: DataKey) {
        userDefaults.set(value, forKey: key.rawValue)
    }

    func getValue(forKey key: DataKey) -> Float {
        if let value = userDefaults.object(forKey: key.rawValue) as? Float {
            return value
        }
        return 0
    }

    func isNotFirstTime() -> Bool {
        let firstTimeKey = DataKey.firstTime.rawValue
        let isFirstTime = userDefaults.bool(forKey: firstTimeKey)
        return isFirstTime
    }

    func didPassFirstTime() {
        let firstTimeKey = DataKey.firstTime.rawValue
        userDefaults.set(true, forKey: firstTimeKey)
    }
}
