//
//  BuyProtocols.swift
//  StoneWallet
//
//  Created by Douglas Pinheiro Mandarino on 14/01/18.
//  Copyright © 2018 Douglas Mandarino. All rights reserved.
//

// MARK: - Model

protocol BuyModelProtocol {
    weak var output: BuyModelOutputProtocol? { get set }

    func fetchBitcoin()
    func buyCoin(quantity: Float)
    func sellCoin(quantity: Float)
}

protocol BuyModelOutputProtocol: class {
    func presentBitCoinValues(_ bitcoin: Bitcoin)
    func presentAmount(withValue value: Float)
}

// MARK: - Presenter

protocol BuyPresenterProtocol {
    var model: BuyModelProtocol? { get set }
    weak var output: BuyPresenterOutputProtocol? { get set }

    func fetchCoin()
    func buyCoin(quantity: String)
    func sellCoin(quantity: String)
}

protocol BuyPresenterOutputProtocol: class {
    func showBitcoinValue(withValues values: [String: Float])
    func showAmount(value: Float)
}

// MARK: - View

protocol BuyViewProtocol {
    var presenter: BuyPresenterProtocol? { get set }
}

// MARK: - Configurator

protocol BuyConfiguratorProtocol {
    func configure(viewController: BuyViewController)
}
