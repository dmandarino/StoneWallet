//
//  PriceConfiguratorTests.swift
//  StoneWalletTests
//
//  Created by Douglas Mandarino on 25/12/17.
//  Copyright © 2017 Douglas Mandarino. All rights reserved.
//

import XCTest
@testable import StoneWallet

class PriceConfiguratorTests: XCTestCase {

    var sut: PriceConfiguratorProtocol?

    override func setUp() {
        super.setUp()
        sut = PriceConfigurator()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testPriceStackConfiguration() {
        let viewController = PriceViewController()
        sut?.configure(viewController: viewController)

        XCTAssertNotNil(viewController.presenter)
        XCTAssertNotNil(viewController.presenter?.model)
        XCTAssertNotNil(viewController.presenter?.model?.output)
        XCTAssertNotNil(viewController.presenter?.output)
    }
}
