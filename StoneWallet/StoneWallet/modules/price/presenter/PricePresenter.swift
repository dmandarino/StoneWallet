//
//  PricePresenter.swift
//  StoneWallet
//
//  Created by Douglas Mandarino on 25/12/17.
//  Copyright © 2017 Douglas Mandarino. All rights reserved.
//

import Foundation

// MARK: - PricePresenter

class PricePresenter {

    var model: PriceModelProtocol?
    weak var output: PricePresenterOutputProtocol?
}

// MARK: - PricePresenterProtocol

extension PricePresenter: PricePresenterProtocol {

    func fetchCoin() {
        model?.fetchBitcoin()
    }
}

// MARK: - PriceModelOutputProtocol

extension PricePresenter: PriceModelOutputProtocol {

    func presentBitCoinValues(_ bitcoin: Bitcoin) {
        var coinValues: [String: Float] = [:]
        coinValues["last"] = bitcoin.last
        coinValues["sell"] = bitcoin.sell
        coinValues["buy"] = bitcoin.buy
        output?.showCoinValues(withValues: [coinValues])
    }

    func presentWallet(withAmount amount: Float, bitcoinQtd: Float, andRealQtd realQtd: Float) {
        output?.updateWalletValues(amount: amount, bitcoinQtd: bitcoinQtd, realQtd: realQtd)
    }
}
