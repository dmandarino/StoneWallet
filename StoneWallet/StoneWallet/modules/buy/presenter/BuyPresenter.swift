//
//  BuyPresenter.swift
//  StoneWallet
//
//  Created by Douglas Pinheiro Mandarino on 14/01/18.
//  Copyright © 2018 Douglas Mandarino. All rights reserved.
//

import Foundation

// MARK: - BuyPresenter

class BuyPresenter {
    var model: BuyModelProtocol?
    var output: BuyPresenterOutputProtocol?
}

// MARK: - BuyPresenterProtocol

extension BuyPresenter: BuyPresenterProtocol {
    func fetchCoin() {
        model?.fetchBitcoin()
    }

    func buyCoin(quantity: String) {
        if let quantityAsFloat = Float(quantity) {
            model?.buyCoin(quantity: quantityAsFloat)
        }
    }

    func sellCoin(quantity: String) {
        if let quantityAsFloat = Float(quantity) {
            model?.sellCoin(quantity: quantityAsFloat)
        }
    }
}

// MARK: - BuyModelOutputProtocol

extension BuyPresenter: BuyModelOutputProtocol {

    func presentBitCoinValues(_ bitcoin: Bitcoin) {
        var coinValues:[String: Float] = [:]
        coinValues["sell"] = bitcoin.sell
        coinValues["buy"] = bitcoin.buy
        output?.showBitcoinValue(withValues: coinValues)
    }

    func presentAmount(withValue value: Float) {
        output?.showAmount(value: value)
    }
}
