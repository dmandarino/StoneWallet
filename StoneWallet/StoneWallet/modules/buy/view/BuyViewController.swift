//
//  BuyViewController.swift
//  StoneWallet
//
//  Created by Douglas Pinheiro Mandarino on 11/01/18.
//  Copyright © 2018 Douglas Mandarino. All rights reserved.
//

import Foundation
import UIKit

// MARK: - BuyViewController

class BuyViewController: UIViewController, BuyViewProtocol {

    var presenter: BuyPresenterProtocol?

    @IBOutlet weak var buyInput: UITextField!
    @IBOutlet weak var sellPriceLabel: UILabel!
    @IBOutlet weak var buyPriceLabel: UILabel!
    @IBOutlet weak var amountLabel: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        configure()
    }

    override func viewWillAppear(_ animated: Bool) {
        presenter?.fetchCoin()
    }

    @IBAction func buyButtonTapped(_ sender: Any) {
        if let quantity = buyInput.text {
            presenter?.buyCoin(quantity: quantity)
        }
    }
    @IBAction func sellButtonTapped(_ sender: Any) {
        if let quantity = buyInput.text {
            presenter?.sellCoin(quantity: quantity)
        }
    }

    private func configure() {
        let configurator: BuyConfiguratorProtocol = BuyConfigurator()
        configurator.configure(viewController: self)
    }
}

// MARK: - BuyPresenterOutputProtocol

extension BuyViewController: BuyPresenterOutputProtocol {

    func showBitcoinValue(withValues values: [String: Float]) {
        sellPriceLabel.text = values["sell"]?.asCurrency
        buyPriceLabel.text = values["buy"]?.asCurrency
    }

    func showAmount(value: Float) {
        amountLabel.text = value.asCurrency
    }
}
