//
//  EnvironmentTests.swift
//  StoneWalletTests
//
//  Created by Douglas Mandarino on 22/12/17.
//  Copyright © 2017 Douglas Mandarino. All rights reserved.
//

import XCTest
import SwiftyPlistManager
@testable import StoneWallet

class EnvironmentTests: XCTestCase {

    override func setUp() {
        super.setUp()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testMercadoBitCoinURLMapped() {
        let MercadoBitCoinURL = "MercadoBitCoinURL"
        guard let endpoint = SwiftyPlistManager.shared.fetchValue(for: MercadoBitCoinURL,
                                                                  fromPlistWithName: "Environment") as? String else {
            XCTFail("failed to fetch bitcoin")
            return
        }
        XCTAssertEqual(endpoint, "https://www.mercadobitcoin.net/api/BTC/ticker/")
    }

    func testFaileureInFetchAValue() {
        let key = "failure"
        guard SwiftyPlistManager.shared.fetchValue(for: key, fromPlistWithName: "Environment") != nil else {
            return
        }
        XCTFail("should've return in guard let")
    }
}
