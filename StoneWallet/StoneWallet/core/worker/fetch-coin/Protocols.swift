//
//  Protocols.swift
//  StoneWallet
//
//  Created by Douglas Pinheiro Mandarino on 14/01/18.
//  Copyright © 2018 Douglas Mandarino. All rights reserved.
//

import Foundation

protocol FetchCoinWorkerProtocol {
    func fetchBitcoin()
}

protocol FetchCoinWorkerOutputProtocol: class {
    func didFetchBitcoin(bitcoin: Bitcoin)
}
