//
//  WalletWorker.swift
//  StoneWallet
//
//  Created by Douglas Pinheiro Mandarino on 17/01/18.
//  Copyright © 2018 Douglas Mandarino. All rights reserved.
//

import Foundation

protocol WalletWorkerProtocol {
    func getWallet() -> Wallet
    func saveWallet(_ wallet: Wallet)
}

struct WalletWorker: WalletWorkerProtocol {

    private let dataManager: DataManagerProtocol?

    init() {
        dataManager = DataManager()
    }

    func getWallet() -> Wallet {
        if let bitcoinQtd = dataManager?.getValue(forKey: DataKey.bitcoinQtd),
           let currency = dataManager?.getValue(forKey: DataKey.currency),
           let isNotFirstTime = dataManager?.isNotFirstTime() {
            if isNotFirstTime {
                return Wallet(bitcoinQtd: bitcoinQtd, currency: currency)
            }
        }
        return createWallet()
    }

    func saveWallet(_ wallet: Wallet) {
        dataManager?.save(value: wallet.bitcoinQtd, forKey: DataKey.bitcoinQtd)
        dataManager?.save(value: wallet.currency, forKey: DataKey.currency)
    }

    private func createWallet() -> Wallet {
        let wallet = Wallet(bitcoinQtd: 0, currency: 100000)
        saveWallet(wallet)
        dataManager?.didPassFirstTime()
        return wallet
    }
}
