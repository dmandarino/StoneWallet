//
//  BuyModelTests.swift
//  StoneWalletTests
//
//  Created by Douglas Pinheiro Mandarino on 15/01/18.
//  Copyright © 2018 Douglas Mandarino. All rights reserved.
//

import XCTest
import UIKit
@testable import StoneWallet

class BuyModelTests: XCTestCase {

    var sut: BuyModel?

    override func setUp() {
        super.setUp()
        sut = BuyModel()
    }

    override func tearDown() {
        super.tearDown()
    }

    func testFetchBitcoin() {
        let expectation = XCTestExpectation(description: "Test call worker to fetch Bitcoin")
        let worker = WorkerMock(expectation: expectation)
        let bitcoin = Bitcoin(high: 0, low: 0, last: 0, buy: 5, sell: 0)
        sut = BuyModel(worker: worker, bitcoin: bitcoin, walletWorker: WalletWorker())
        sut?.fetchBitcoin()
        wait(for: [expectation], timeout: 10)
    }

    func testDidFetchBitcoin() {
        let expectation = XCTestExpectation(description: "Test call model to present Bitcoin")
        let presenter = BuyPresenterMock(expectation: expectation)
        sut?.output = presenter
        sut?.fetchBitcoin()
        wait(for: [expectation], timeout: 10)
    }

    func testDidFetchBitcoinAndShowWalletAmount() {
        let expectation = XCTestExpectation(description: "Test call model to present Wallet Amount")
        let presenter = BuyPresenterMock(expectation: expectation)
        sut?.output = presenter
        sut?.fetchBitcoin()
        wait(for: [expectation], timeout: 10)
    }

    func testBuyCoin() {
        let bitcoin = Bitcoin(high: 0, low: 0, last: 0, buy: 5, sell: 0)
        let dataManager: DataManagerProtocol = DataManager()
//        UserDefaults.standard.set(false, forKey: DataKey.firstTime.rawValue)
        dataManager.save(value: 10, forKey: DataKey.bitcoinQtd)
        dataManager.save(value: 1000, forKey: DataKey.currency)
        dataManager.didPassFirstTime()
        sut = BuyModel(worker: FetchCoinWorker(delegate: sut!), bitcoin: bitcoin, walletWorker: WalletWorker())
        sut?.buyCoin(quantity: 100)
        XCTAssertEqual(sut?.wallet?.bitcoinQtd, Float(30))
        XCTAssertEqual(sut?.wallet?.currency, Float(900))
    }

    func testBuyCoinFailure() {
        let bitcoin = Bitcoin(high: 0, low: 0, last: 0, buy: 5, sell: 0)
        let dataManager: DataManagerProtocol = DataManager()
        dataManager.save(value: 10, forKey: DataKey.bitcoinQtd)
        dataManager.save(value: 1000, forKey: DataKey.currency)
        dataManager.didPassFirstTime()
        sut = BuyModel(worker: FetchCoinWorker(delegate: sut!), bitcoin: bitcoin, walletWorker: WalletWorker())
        sut?.buyCoin(quantity: 2000)
        XCTAssertEqual(sut?.wallet?.bitcoinQtd, Float(10))
        XCTAssertEqual(sut?.wallet?.currency, Float(1000))
    }

    func testSellCoin() {
        let bitcoin = Bitcoin(high: 0, low: 0, last: 0, buy: 0, sell: 10)
        let dataManager: DataManagerProtocol = DataManager()
        dataManager.save(value: 10, forKey: DataKey.bitcoinQtd)
        dataManager.save(value: 1000, forKey: DataKey.currency)
        dataManager.didPassFirstTime()
        sut = BuyModel(worker: FetchCoinWorker(delegate: sut!), bitcoin: bitcoin, walletWorker: WalletWorker())
        sut?.sellCoin(quantity: 10)
        XCTAssertEqual(sut?.wallet?.bitcoinQtd, Float(0))
        XCTAssertEqual(sut?.wallet?.currency, Float(1100))
    }

    func testSellCoinFailure() {
        let bitcoin = Bitcoin(high: 0, low: 0, last: 0, buy: 0, sell: 10)
        let dataManager: DataManagerProtocol = DataManager()
        dataManager.save(value: 10, forKey: DataKey.bitcoinQtd)
        dataManager.save(value: 100, forKey: DataKey.currency)
        dataManager.didPassFirstTime()
        sut = BuyModel(worker: FetchCoinWorker(delegate: sut!), bitcoin: bitcoin, walletWorker: WalletWorker())
        sut?.sellCoin(quantity: 100)
        XCTAssertEqual(sut?.wallet?.bitcoinQtd, Float(10))
        XCTAssertEqual(sut?.wallet?.currency, Float(100))
    }
}

private class WorkerMock: FetchCoinWorkerProtocol {

    private let expectation: XCTestExpectation?

    init(expectation: XCTestExpectation) {
        self.expectation = expectation
    }

    func fetchBitcoin() {
        expectation?.fulfill()
    }

}

private class BuyPresenterMock: BuyModelOutputProtocol {

    private let expectation: XCTestExpectation?

    init(expectation: XCTestExpectation) {
        self.expectation = expectation
    }

    func presentAmount(withValue value: Float) {
        if expectation?.description == "Test call model to present Wallet Amount" {
            expectation?.fulfill()
        }
    }

    func presentBitCoinValues(_ bitcoin: Bitcoin) {
        if expectation?.description == "Test call model to present Bitcoin" {
            expectation?.fulfill()
        }
    }
}
